package com.alore.poc.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    JdbcOperations jdbcOperations;
    @Autowired
    NamedParameterJdbcOperations namedParameterJdbcOperations;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Override
    public int saveUserInfoToUserTable(String userId, String userName, String password, String phone, String email, String gender) {
        String query = String.format("INSERT INTO user(user_id,user_name,password,email,phone,gender) VALUES (:userId,:userName,:password,:phone,:email,:gender");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("userId", userId);
        ((MapSqlParameterSource) namedParameters).addValue("userName", userName);
        ((MapSqlParameterSource) namedParameters).addValue("password", password);
        ((MapSqlParameterSource) namedParameters).addValue("phone", phone);
        ((MapSqlParameterSource) namedParameters).addValue("email", email);
        ((MapSqlParameterSource) namedParameters).addValue("gender", gender);

        return namedParameterJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public int updateUserInfoToUserTable(String userId, String userName, String password, String phone, String email, String gender) {
        String query = String.format("UPDATE user SET user_name=:userName,password=:password,email=:email,phone=:phone,gender=:gender where user-id=(:userId)");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("userId", userId);
        ((MapSqlParameterSource) namedParameters).addValue("userName", userName);
        ((MapSqlParameterSource) namedParameters).addValue("password", password);
        ((MapSqlParameterSource) namedParameters).addValue("phone", phone);
        ((MapSqlParameterSource) namedParameters).addValue("email", email);
        ((MapSqlParameterSource) namedParameters).addValue("gender", gender);

        return namedParameterJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public int saveReviewDetail(String userId, String hotelId, String comment, String rating) {
        String query = String.format("INSERT INTO user_hotel_mapping(user_id,hotel_id,comment,rating) VALUES (:userId,:hotelId,:comment,:rating");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("userId", userId);
        ((MapSqlParameterSource) namedParameters).addValue("hotelId", hotelId);
        ((MapSqlParameterSource) namedParameters).addValue("comment", comment);
        ((MapSqlParameterSource) namedParameters).addValue("rating", rating);

        return namedParameterJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public int removeReviewDetail(String userId, String hotelId) {
        String query = String.format("DELETE FROM  user_hotel_mapping where user_id=:userId and hotel_id=:hotelId");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("userId", userId);
        ((MapSqlParameterSource) namedParameters).addValue("hotelId", hotelId);

        return namedParameterJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public int removeUserDetail(String userId) {
        String query = String.format("DELETE FROM  user where user_id=:userId");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("userId", userId);
        return namedParameterJdbcTemplate.update(query, namedParameters);
    }
}
