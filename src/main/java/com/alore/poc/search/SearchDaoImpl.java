package com.alore.poc.search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SearchDaoImpl implements SearchDao {
    @Autowired
    private JdbcOperations jdbcOperations;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List searchCity(String cityName) {
        String query = "SELECT h.city FROM hotel h where h.city like lower(concat('%',?,'%'))";
        return jdbcOperations.queryForList(query, new Object[]{cityName});
    }
}
