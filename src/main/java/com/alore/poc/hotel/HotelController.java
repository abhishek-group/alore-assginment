package com.alore.poc.hotel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelController {
    @Autowired
    HotelService hotelService;
/*
    API to add the Hotel information
 */
    @PostMapping("/api/v1/hotel/add")
    public ResponseEntity<?> addHotel(@RequestBody String request){

        return hotelService.addHotel(request);
    }
 /*
     API to update the Hotel information
  */
    @PostMapping("/api/v1/hotel/update")
    public ResponseEntity<?> updateHotel(@RequestBody String request){

        return hotelService.updateHotel(request);
    }
    /*
         API to delete the Hotel information
      */
    @PostMapping("/api/v1/hotel/delete")
    public ResponseEntity<?> removeHotel(@RequestBody String request){

        return hotelService.removeHotel(request);
    }


}
