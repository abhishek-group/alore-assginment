package com.alore.poc.hotel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class HotelDaoImpl  implements HotelDao{

    @Autowired
    JdbcOperations jdbcOperations;
    @Autowired
    NamedParameterJdbcOperations namedParameterJdbcOperations;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Override
    public int saveHotelInfoToHotelTable(String hotelId, String hotelName, boolean ac, boolean wifi, int noOfStar, String address, String city, String state, String pinCode) {
        String query = String.format("INSERT INTO hotel(hotel_id,hotel_name,ac,wifi,no_of_start,city,state,pin_code) VALUES (:hotelId,:hotelName,:ac,:wifi,:noOfStar,:address,:city,:state,:pinCode");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("hotelId", hotelId);
        ((MapSqlParameterSource) namedParameters).addValue("hotelName", hotelName);
        ((MapSqlParameterSource) namedParameters).addValue("ac", ac);
        ((MapSqlParameterSource) namedParameters).addValue("wifi", wifi);
        ((MapSqlParameterSource) namedParameters).addValue("noOfStar", noOfStar);
        ((MapSqlParameterSource) namedParameters).addValue("address", address);
        ((MapSqlParameterSource) namedParameters).addValue("city", city);
        ((MapSqlParameterSource) namedParameters).addValue("state", state);
        ((MapSqlParameterSource) namedParameters).addValue("pinCode", pinCode);

        return namedParameterJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public int updateHotelInfoToHotelTable(String hotelId, String hotelName, boolean ac, boolean wifi, int noOfStar, String address, String city, String state, String pinCode) {
        String query = String.format("UPDATE hotel SET hotel_id=:,hotel_name=:hotelName,ac=:ac,wifi=:wifi,no_of_start=:noOfStar,address=:address,city=:city,state=:state,pin_code=:pinCode where hotel_id=(:hotelId)");
        SqlParameterSource namedParameters = new MapSqlParameterSource();
        ((MapSqlParameterSource) namedParameters).addValue("hotelId", hotelId);
        ((MapSqlParameterSource) namedParameters).addValue("hotelName", hotelName);
        ((MapSqlParameterSource) namedParameters).addValue("ac", ac);
        ((MapSqlParameterSource) namedParameters).addValue("wifi", wifi);
        ((MapSqlParameterSource) namedParameters).addValue("noOfStar", noOfStar);
        ((MapSqlParameterSource) namedParameters).addValue("address", address);
        ((MapSqlParameterSource) namedParameters).addValue("city", city);
        ((MapSqlParameterSource) namedParameters).addValue("state", state);
        ((MapSqlParameterSource) namedParameters).addValue("pinCode", pinCode);

        return namedParameterJdbcTemplate.update(query, namedParameters);
    }
}
