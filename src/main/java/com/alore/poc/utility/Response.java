package com.alore.poc.utility;

import org.springframework.stereotype.Component;

@Component
public class Response {

    private boolean success;
    private Object payload;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
