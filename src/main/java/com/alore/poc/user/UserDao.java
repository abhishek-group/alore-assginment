package com.alore.poc.user;

public interface UserDao {

     int saveUserInfoToUserTable(String userId,String userName,String password,String phone,String email,String gender);

     int updateUserInfoToUserTable(String userId,String userName,String password,String phone,String email,String gender);

     int saveReviewDetail(String userId,String hotelId,String comment,String rating);

     int removeReviewDetail(String userId,String hotelId);

     int removeUserDetail(String userId);


}
