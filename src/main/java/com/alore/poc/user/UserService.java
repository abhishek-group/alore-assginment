package com.alore.poc.user;

import com.alore.poc.utility.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    UserDao userDao;
    @Autowired
    Response response;

    public ResponseEntity<?> createUser(String request){
        JSONObject jsonReq=new JSONObject(request);
        String userId= UUID.randomUUID().toString();
        String userName=jsonReq.getString("userName");
        String password=jsonReq.getString("password");
        String email=jsonReq.getString("email");
        String phone=jsonReq.getString("phone");
        String gender=jsonReq.getString("gender");


        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=userDao.saveUserInfoToUserTable(userId,userName,password,email,phone,gender);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to insert in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully  inserted into  database.");
        }
        return ResponseEntity.ok(response);

    }
    public ResponseEntity<?> updateUser(@RequestBody String request){
        JSONObject jsonReq=new JSONObject(request);
        String userId= jsonReq.getString("userId");
        String userName=jsonReq.getString("userName");
        String password=jsonReq.getString("password");
        String email=jsonReq.getString("email");
        String phone=jsonReq.getString("phone");
        String gender=jsonReq.getString("gender");


        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=userDao.updateUserInfoToUserTable(userId,userName,password,email,phone,gender);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to insert in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully  inserted into  database.");
        }
        return ResponseEntity.ok(response);
    }
    public ResponseEntity<?> addReview(@RequestBody String request){
        JSONObject jsonReq=new JSONObject(request);
        String userId= jsonReq.getString("userId");
        String hotelId=jsonReq.getString("hotelId");
        String comment=jsonReq.getString("comment");
        String rating=jsonReq.getString("rating");

        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=userDao.saveReviewDetail(userId,hotelId,comment,rating);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to store in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully  stored into  database.");
        }
        return ResponseEntity.ok(response);
    }
    public ResponseEntity<?> deleteReview( String request){
        JSONObject jsonReq=new JSONObject(request);
        String userId= jsonReq.getString("userId");
        String hotelId=jsonReq.getString("hotelId");

        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=userDao.removeReviewDetail(userId,hotelId);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to store in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully  stored into  database.");
        }
        return ResponseEntity.ok(response);

    }
    public ResponseEntity<?> deleteUser( String request){
        JSONObject jsonReq=new JSONObject(request);
        String userId= jsonReq.getString("userId");

        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=userDao.removeUserDetail(userId);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to store in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully  stored into  database.");
        }
        return ResponseEntity.ok(response);

    }

}
