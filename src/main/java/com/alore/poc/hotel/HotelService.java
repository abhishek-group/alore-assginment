package com.alore.poc.hotel;

import com.alore.poc.utility.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class HotelService {
    @Autowired
    HotelDao hotelDao;
    @Autowired
    Response response;

    public ResponseEntity<?> addHotel(String request){
        JSONObject jsonReq=new JSONObject(request);
        String hotelId= UUID.randomUUID().toString();
        String hotelName=jsonReq.getString("hotelName");
        boolean ac=jsonReq.getBoolean("ac");
        boolean wifi=jsonReq.getBoolean("wifi");
        int noOfStar=jsonReq.getInt("noOfStar");
        String address=jsonReq.getString("address");
        String city=jsonReq.getString("city");
        String state=jsonReq.getString("state");
        String pinCode=jsonReq.getString("pinCode");


        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=hotelDao.saveHotelInfoToHotelTable(hotelId,hotelName,ac,wifi,noOfStar,address,city,state,pinCode);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to insert in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully saved into database.");
        }
        return ResponseEntity.ok(response);


    }

    public ResponseEntity<?> updateHotel(String request){
        JSONObject jsonReq=new JSONObject(request);
        String hotelId= jsonReq.getString("hotelId");
        String hotelName=jsonReq.getString("hotelName");
        boolean ac=jsonReq.getBoolean("ac");
        boolean wifi=jsonReq.getBoolean("wifi");
        int noOfStar=jsonReq.getInt("noOfStar");
        String address=jsonReq.getString("address");
        String city=jsonReq.getString("city");
        String state=jsonReq.getString("state");
        String pinCode=jsonReq.getString("pinCode");


        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=hotelDao.updateHotelInfoToHotelTable(hotelId,hotelName,ac,wifi,noOfStar,address,city,state,pinCode);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to update in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully updated the information.");
        }
        return ResponseEntity.ok(response);
    }
    public ResponseEntity<?> removeHotel(String request){
        JSONObject jsonReq=new JSONObject(request);
        String hotelId= jsonReq.getString("hotelId");

        Map<String ,Object> payload=new LinkedHashMap<>();
        int count=0;
        count=hotelDao.deleteHotel(hotelId);

        if(count==0){
            response.setSuccess(false);
            response.setPayload(payload);
            payload.put("message","failed to update in database.");
        }else {
            response.setSuccess(true);
            response.setPayload(payload);
            payload.put("message","successfully updated the information.");
        }
        return ResponseEntity.ok(response);

    }
}
