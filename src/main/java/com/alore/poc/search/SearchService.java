package com.alore.poc.search;
import com.alore.poc.utility.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class SearchService {
    @Autowired
    SearchDao searchDao;
    @Autowired
    Response response;


    public ResponseEntity<?> fetchCity(@RequestParam String cityName){
        List list = null;
        list = searchDao.searchCity(cityName);
        LinkedHashMap<String, Object> serialList = new LinkedHashMap<>();
        serialList.put("data", list);
        response.setSuccess(true);
        response.setPayload(serialList);
        return new ResponseEntity(response, HttpStatus.OK);

    }

}
