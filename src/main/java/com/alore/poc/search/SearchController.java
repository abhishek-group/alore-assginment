package com.alore.poc.search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {
    @Autowired
    SearchService searchService;

    @GetMapping("/api/v1/city/search")
    public ResponseEntity<?> fetchCity(@RequestParam String cityName)
    {
        return searchService.fetchCity(cityName);
    }



}
