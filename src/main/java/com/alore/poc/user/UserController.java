package com.alore.poc.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    UserService userService;
    /*
         API to add the User information
      */
    @PostMapping("/api/v1/user/add")
    public ResponseEntity<?> createUser(@RequestBody String request){

        return userService.createUser(request);
    }
    /*
         API to update the User information
      */
    @PostMapping("/api/v1/user/update")
    public ResponseEntity<?> updateUser(@RequestBody String request){

        return userService.updateUser(request);
    }
    /*
     API to create the rating and comment.
  */
    @PostMapping("/api/v1/user/hotel/comment/rating/add")
    public ResponseEntity<?> addReview(@RequestBody String request){

        return userService.addReview(request);
    }
    /*
     API to delete the rating and comment information
  */
    @PostMapping("/api/v1/user/hotel/comment/rating/delete")
    public ResponseEntity<?> deleteReview(@RequestBody String request){

        return userService.deleteReview(request);
    }
    /*
     API to update the user information
  */
    @PostMapping("/api/v1/user/delete")
    public ResponseEntity<?> deleteUser(@RequestBody String request){

        return userService.deleteUser(request);
    }





}
