package com.alore.poc.hotel;

public interface HotelDao {

    int saveHotelInfoToHotelTable(String hotelId,String hotelName,boolean ac,boolean wifi,int noOfStar,String address,String city,String state,String pinCode);

    int updateHotelInfoToHotelTable(String hotelId,String hotelName,boolean ac,boolean wifi,int noOfStar,String address,String city,String state,String pinCode);

    int deleteHotel(String hotelId);
}
